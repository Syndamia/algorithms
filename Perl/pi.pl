#!/usr/bin/perl
#!/data/data/com.termux/files/usr/bin/perl

if (grep(/^$ARGV[0]$/, ('h', '-h', '--help'))) {
    print "m.pl MAX_VALUE TOTAL_PAIRS\n";
    exit;
}

($max_val, $total) = (shift // 120, shift // 500);

print "PI estimate: " . est_pi($max_val, $total) . " with $total numbers between 0 and $max_val.\n";

sub est_pi($$) {
    my ($MAX_VAL, $TOTAL) = @_;
    my $total_coprime;

    foreach (1 .. $TOTAL) {
        my $gcd = gcd_iter(rnd_int($MAX_VAL), rnd_int($MAX_VAL));

        $total_coprime += ($gcd == 1);
    }
        
    return sqrt(6/($total_coprime/$TOTAL));
}

sub rnd_int($) {
    return int(rand(shift) + 0.5);
}

sub gcd_iter($$) {
  my ($u, $v) = @_;
  while ($v) {
    ($u, $v) = ($v, $u % $v);
  }
  return abs($u);
}
