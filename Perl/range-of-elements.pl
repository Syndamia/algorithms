#!/usr/bin/perl

# Impliment a method that will determinate if exists a range of
# non-negative elements in an array that sum up to a specific non-negative number
#  
# Example 1 - targetSum 3, numbers = [1, 7, 1, 1 ,1, 5, 6, 1] - output true (the sum of the elemtns in range 2 to 4 is 3)
# Example 2 = targetSum 7, numbers = [0, 4, 5, 1, 8, 9, 12, 3, 1] - output false (no range sums to 7)

$target_sum = shift // 4;
@numbers    = (@ARGV > 0) ? @ARGV : (10, 9, 8, 6, 5, 4) ;
$range_exists = False;

$start = 0;
$end   = 1;
$sum   = $numbers[0] + $numbers[1];

while ($end < int(@numbers)) {
    if ($sum == $target_sum) {
        $range_exists = True;
        last;
    }
    elsif ($sum > $target_sum) {
        $sum -= $numbers[$start++];
    }
    else {
        $sum += $numbers[++$end];
    }
}
print $range_exists . "\n";
