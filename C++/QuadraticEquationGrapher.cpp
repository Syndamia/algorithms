#include <iostream>
#include <math.h>

using namespace std;

/* Program that graphs a quadratic equation on the standard output */

/* Equation precision - how precise the quadratic equation result should be
 * - higher values are more precise, but they could not show up on the graph
 * - needs to be balanced with step
 * Step - size of each unit (character)
 */
const int eqPrec  = 1;
const double step = 1;

const char line    = '*';
const char verLine = '|';
const char horLine = '-';
const char empty   = ' ';

double quadr(double x, double a, double b, double c) {
    return double( round( ( (a*pow(x, 2)) + (b*x) + c) * eqPrec ) / eqPrec);
}

void printQadr(int startx, int endx, int starty, int endy, double a, double b, double c) {
    double currY, currX;
    for (int y = max(starty, endy); y >= min(starty, endy); y--) {
        for (int x = startx; x <= endx; x++) {
            currY = y * step;
            currX = x * step;

            if (quadr(currX, a, b, c) == currY 
                || ((quadr(currX, a, b, c) < currY 
                    || (quadr(currX - step, a, b, c) == 0 || quadr(currX + step, a, b, c) == 0)) // Wide lines
                   && (quadr(currX + step, a, b, c) > currY != quadr(currX - step, a, b, c) > currY)) // Long lines  
            ) {
                cout << line;
            } else if (x == 0) {
                cout << verLine;
            } else if (y == 0) {
                cout << horLine;
            } else {
                cout << empty;
            }
        }
        cout << endl;
    }
}

void printQadr(int start, int end, double a, double b, double c) {
    printQadr(start, end, start, end, a, b, c);
}

int main()
{
   double a = 0.2;
   double b =   2;
   double c = -10;
   
   printQadr(-25, 15, 20, -15, a, b, c);

   return 0;
}
