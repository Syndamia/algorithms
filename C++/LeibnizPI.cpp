#include <iostream>

using namespace std;

int main()
{
	cout << "n = ";
	unsigned int n = 1;
	cin >> n;
	long double pid4 = 1;

	for(unsigned int i = 0, dvsr = 3; i < n; i++, dvsr += 2) {
		pid4 += ((i % 2) ? 1.0 : -1.0) / dvsr;
	}

	cout.precision(64);
	cout << endl << pid4 * 4.0 << endl;
   
	return 0;
}

/* This is a quick implementation of of the Leibniz formula for PI
 * https://en.wikipedia.org/wiki/Leibniz_formula_for_%CF%80
 *
 * Fun fact: you need around 100 million iterations for 7 correct decimal places
 */
