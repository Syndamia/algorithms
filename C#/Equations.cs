using System;
using System.Linq; // used only for the input

namespace Equasions {
    class MainClass {
        /* Algorithm that solves a system of equation of the type
         * | a1 * x + b1 * y = c1
         * | a2 * x + b2 * y = c2
         * via substitution.
         * 
         * At the bottom you can find test cases.        
         */       
        public static void Main(string[] args) {
            double[] input = Console.ReadLine().Split().Select(double.Parse).ToArray(); // a1 b1 c1 a2 b2 c2

            double[] parm = { input[0], input[3], input[1], input[4] }; // a1 a2 b1 b2
            double[] eres = { input[2], input[5] }; // c1 c2

            double m = 0, n = 0;
            string result = null;

            for (int i = 0; i < parm.Length; i++) {
                for (int j = 0; j < parm.Length; j++) {
                    if (parm[i] != 0 || parm[j] != 0) {
                        continue;
                    }

                    result = (i < 2) ? "n" : "m";

                    if (i == j) {
                        m = eres[i % 2] / parm[(i + 2) % 4];
                        n = (eres[(i + 1) % 2] - parm[3 - i] * m) / parm[(5 - i) % 4];
                    }
                    else if (i + j == 3) {
                        m = eres[Math.Min(i, j)] / parm[Math.Min(i, j) + 2];
                        n = eres[Math.Max(i, j) - 2] / parm[Math.Max(i, j) - 2];
                    }
                    else {
                        result = "No solution!";
                    }

                    if ((!double.IsNaN(m) && !double.IsInfinity(m)) || 
                        (!double.IsNaN(n) && !double.IsInfinity(n)) ||
                        result.Length > 1) // if m or n have normal values or if result is "No solution!"
                    {
                        j = i = parm.Length; // "breaking" both cycles
                    }
                }
            }

            if (result == null) {
                m = (parm[0] * eres[1] - parm[1] * eres[0]) / (parm[0] * parm[3] - parm[1] * parm[2]);
                n = (eres[0] - parm[2] * m) / parm[0];

                result = "n";
            }

            if (double.IsInfinity(n) || double.IsInfinity(m)) {
                result = "No solution!";
            }
            else if (result == "n") {
                result = $"X: {(double.IsNaN(n) ? "Any" : n + "")}  Y: {(double.IsNaN(m) ? "Any" : m + "")}";
            }
            else if (result == "m") {
                result = $"X: {(double.IsNaN(m) ? "Any" : m + "")}  Y: {(double.IsNaN(n) ? "Any" : n + "")}";
            }

            Console.WriteLine(result);
        }
    }
}

/* Example tests (numbers are ordered a1 b1 c1 a2 b2 c2) that:
 * 
 * I. Have a solution (solution is in brackets as (x ; y)):
 *      5 -2 1 1 3 7 ( 1; 2) no zeroes
 *      0 1 8 1 1 5 (-3; 8) a1 = 0
 *      1 0 8 1 1 5 ( 8;-3) a2 = 0 
 *      1 1 8 0 1 5 ( 3; 5) b1 = 0
 *      1 1 8 1 0 5 ( 5; 3) b2 = 0
 *      0 1 3 1 0 9 ( 9; 3) a1 = 0 and b2 = 0
 *      1 0 3 0 1 9 ( 3; 9) a2 = 0 and b1 = 0
 *      0 2 8 0 1 4 (any; 4) a1 = 0 and a2 = 0
 *      2 0 8 1 0 4 ( 4;any) b1 = 0 and b2 = 0
 *      0 0 0 0 0 0 (any;any) all are 0
 * 
 * II. Don't have a solution:
 *      0 0 4 1 1 4  a1 = 0 and b1 = 0
 *      1 1 4 0 0 4  a2 = 0 and b2 = 0
 *      0 0 1 0 0 0
 * 
 * Problems with this algorithm:
 * 
 * I. Incomplete answers:
 *      (all cases where all numbers are the same)
 *      2 2 2 2 2 2  Gives (any;any) answer, but that is only correct if it solves the equasion
 */

